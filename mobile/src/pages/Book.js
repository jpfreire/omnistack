import React,{useState, useEffect} from 'react'
import {View,Alert, Text,TextInput, AsyncStorage, SafeAreaView,StyleSheet} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import api from '../services/Api'

export default function Book({navigation}) {

    const id = navigation.getParam('id')

    const [date, setDate] = useState('')

    async function handleSubmit(){
        const user_id = await AsyncStorage.getItem('user')
        await api.post(`/spots/${id}/bookings`,{date},
        {headers: {user_id}})

        Alert.alert('Solicitação de reserva enviada.')
        navigation.navigate('List')

    }

    function handleCancel(){
        navigation.navigate('List')
    }

    return (
        <SafeAreaView style={styles.container}>
            <Text style={styles.label}>Data de interesse *</Text>  
            <TextInput 
            style={styles.input}
            placeholder="Qual a data da reserva?"
            placeholderTextColor="#999"
            autoCapitalize="none"
            autoCorrect={false}
            value={date}
            onChangeText={setDate}
            />
            <TouchableOpacity onPress={handleSubmit} style={styles.button}>
                <Text style={styles.buttonText}>Solicitar reserva</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={handleCancel} style={[styles.button, styles.cancelButton]}>
                <Text style={styles.buttonText}>Cancelar</Text>
            </TouchableOpacity>

            <Text>Book {id}</Text>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 30,
    },
    title: {
        fontSize: 20,
        color: '#444',
        paddingHorizontal: 20,
        marginBottom: 15
    },
    label: {
        marginTop: 30,
        fontWeight: "bold",
        color: "#444",
        marginBottom: 8
    },

    input: {
        borderWidth: 1,
        borderColor: "#ddd",
        paddingHorizontal: 20,
        fontSize: 16,
        color: "#444",
        height: 44,
        marginBottom: 20,
        borderRadius:3
    },

    button: {
        height:42,
        backgroundColor: "#f05a5b",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 3,
    },

    cancelButton: {
        marginTop: 10,
        backgroundColor: "#ccc",

    },

    buttonText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 16
    }
})