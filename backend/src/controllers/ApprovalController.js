const Booking = require('../models/Booking')

module.exports = {
    async store(req,res){
        console.log(req.params)
        const {booking_id}=req.params
        console.log(`aprovando booking ${booking_id}`)
        const booking = await Booking.findById(booking_id).populate('spot')
        if (!booking){
            return res.status(400).json({msg: `Booking não encontrado ${booking_id}`})
        }
        booking.approved = true
        await booking.save()
        return res.json(booking)
    }
}

