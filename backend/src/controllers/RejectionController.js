const Booking = require('../models/Booking')

module.exports = {
    async store(req,res){
        const {booking_id}=req.params
        const booking = await Booking.findById(booking_id).populate('spot')
        if (!booking){
            return res.status(400).json({msg: `Booking não encontrado ${booking_id}`})
        }
        booking.approved = false
        await booking.save()
        return res.json(booking)
    }
}

