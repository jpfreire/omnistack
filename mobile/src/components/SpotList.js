import React, {useEffect, useState} from 'react'
import {withNavigation} from 'react-navigation'
import {View,Text, StyleSheet, Image, FlatList} from 'react-native'

import api from '../services/Api'
import { TouchableOpacity } from 'react-native-gesture-handler'

function SpotList({tech, navigation}) {

    console.log(`SpotList - tech ${tech}`)

    const [spots, setSpots] = useState([])

    useEffect(()=>{
        async function loadSpots(){
            console.log('api.get')
            const resp = await api.get('/spots',{
                params: {tech}
            })
            setSpots(resp.data)
        }
        loadSpots()
    },[])

    function handleNavigation(id){
        navigation.navigate('Book', {id})
    }

    return (
    <View style={styles.container}>
        <Text style={styles.title}> Empresas que usam <Text style={styles.bold}>{tech}</Text></Text>
        <FlatList 
            style={styles.list}
            data={spots}
            keyExtractor={spot => spot._id}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={ ({item}) => (
                <View style={styles.li}>
                    <Image style={styles.thumbnail} source={{ uri: item.thumbnail_url }}/>
                    <Text style={styles.company}>{item.company}</Text>
                    <Text style={styles.price}>{item.price}</Text>
                    <TouchableOpacity onPress={()=>{handleNavigation(item._id)}} style={styles.button}>
                        <Text style={styles.buttonText}>Solicitar Reserva</Text>
                    </TouchableOpacity>
                </View>
            )}
        />
    </View>)   
}

const styles = StyleSheet.create({
    container: {
        marginTop: 30,
    },
    title: {
        fontSize: 20,
        color: '#444',
        paddingHorizontal: 20,
        marginBottom: 15
    },
    bold: {
        fontWeight: 'bold'
    },
    li: {
        paddingHorizontal: 20,

    },
    thumbnail: {
        width: 200,
        height: 120, 
        resizeMode: 'cover',
        borderRadius: 10,
        backgroundColor: '#ddd'
    },
    company: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333',
        marginTop: 10
    },
    price: {
        fontSize: 15,
        color: '#999',
        marginTop: 5
    },
    button: {
        height:32,
        backgroundColor: "#f05a5b",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 3,
    },

    buttonText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 16
    }
})

export default withNavigation(SpotList)