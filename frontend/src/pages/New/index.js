import React, {useState, useMemo} from 'react'
import camera from '../../assets/camera.svg'
import  './styles.css'
import api from '../../services/api'

export default function New({history}){
    const [company, setCompany] = useState('')
    const [techs, setTechs] = useState('')
    const [price, setPrice] = useState('')
    const [thumbnail, setThumbnail] = useState(null)

    const preview = useMemo(
        () => {

            return thumbnail ? URL.createObjectURL(thumbnail) : null
        },
        [ thumbnail ]
    )

    async function handleSubmit(event){
        event.preventDefault()
        const data = new FormData()
        const user_id = localStorage.getItem('user')
        data.append('company',company)
        data.append('techs',techs)
        data.append('price',price)
        data.append('thumbnail',thumbnail)
        const response = api.post('/spots',data,{
            headers: {user_id}
        })
        console.log(response)
        history.push('/dashboard')
    }

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="thumbnailFile" id="thumbnail" 
                    style={{backgroundImage: `url(${preview})`}}
                    className={thumbnail?`has-thumbnail`:``}>
                <input type="file" id="thumbnailFile" 
                        onChange={event => setThumbnail(event.target.files[0])}/>
                <img src={camera} alt="selecione uma imagem"/>
            </label>

            
            <label htmlFor="company">Empresa *</label>
            <input id="company"
            placeholder="Sua empresa"
            value={company}
            onChange={event => setCompany(event.target.value)}/>

            <label htmlFor="techs">Tecnologias* *</label>
            <input id="techs"
            placeholder="Quais tecnologias usam? Ex.: vue, java, react..."
            value={techs}
            onChange={event => setTechs(event.target.value)}/>

            <label htmlFor="price">Diaria * <span> (deixe em branco para gratuito)</span></label>
            <input id="price"
            placeholder="Valor da diaria em Reais."
            value={price}
            onChange={event => setPrice(event.target.value)}/>

            <button className="btn" type="submit">Cadastrar</button>
        </form>
    )
}